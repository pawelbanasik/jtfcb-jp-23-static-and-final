class Thing {

	// sa zmienne i metody typu static

	// final znaczy constant i nie mozna pozniej do niej nic przypisac taka
	// blokada, static dotyczy klasy ktora jest tu akurat tylko
	// jedna
	public final static int LUCKY_NUMBER = 7;

	// instance variable
	public String name;

	// static class variables, dotyczy klasy
	public static String description;

	// count sluzy do liczenia ile obiektow zrobilem
	// count przypisane do klasy przez static
	public static int count = 0;

	public int id;

	// liczenie obiektow ktore stworzylem
	public Thing() {

		id = count;
		count++;

	}

	// dal void bo return type jest empty
	public void showName() {

		System.out.println("Object id: " + id + ", " + description + ": " + name);
	}

	public static void showInfo() {

		System.out.println(description);
		// Error Won't work System.out.println(name);
		// metody typu static nie moga przywolywac instance variables!!!!
	}

}

public class App {

	public static void main(String[] args) {

		// do statica tak sie odnosi
		Thing.description = "I am a thing";
		// System.out.println(Thing.description);
		// wywolujesz metode klasy przez nazwe klasy
		Thing.showInfo();

		System.out.println("Before creating objects, count is: " + Thing.count);

		// instance variables kazdy obiekt mozna zrobic
		Thing thing1 = new Thing();
		Thing thing2 = new Thing();
		
		// do liczenia obiekt�w poprzez count
		System.out.println("After creating objects, count is: " + Thing.count);

		thing1.name = "Bob";
		thing2.name = "Sue";

		// zastepuje je tym ponizej
		// System.out.println(thing1.name);
		// System.out.println(thing2.name);
		thing1.showName();
		thing2.showName();

		// static
		// Thing.showInfo();

		// uzycie statica, tylko taki zapis i mamy liczbe pi
		// .Math ma rozne liczby w podpowiedziach
		// liczby typu constant musza byc duzymi literami
		System.out.println(Math.PI);
		// Error nie da sie przypisac Math.PI = 3;

		System.out.println(Thing.LUCKY_NUMBER);

	}

}
